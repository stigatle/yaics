/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2020 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STATUS_WINDOW_H
#define STATUS_WINDOW_H

#include <QtGui>
#include "qsocialapi/status.h"
#include "messagewindow.h"
#include <qtimer.h>

class StatusWindow : public MessageWindow {
  Q_OBJECT
public:
  StatusWindow(QSocialAPI* sa, const QString& reply_to_user, message_id_t id,
               QWidget* parent=0);
  QSocialAPIRequest* sendMessageRequest(const QString& msg);

private slots:
    void onStatusReady(StatusMessage*);
    void closeEvent(QCloseEvent *);

private:
  message_id_t in_reply_to_id;
};

#endif
