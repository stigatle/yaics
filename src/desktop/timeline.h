/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2020 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TIMELINE_H
#define TIMELINE_H

#include <QScrollArea>
#include <QScrollBar>
#include <QPushButton>

#include "qsocialapi/status.h"
#include "qsocialapi/messagelist.h"
#include "qsocialapi/qsocialapi.h"

#include "avatar.h"
#include "messagewidget.h"

#define RETRIEVE_COUNT 20

class TimeLine : public QScrollArea {
  Q_OBJECT

public:
  TimeLine(const QString& timeLine, QSocialAPI* sa, QWidget* parent=0,
           bool highlight=true, bool dmButton=true, bool isReverse=false);

  void reload(bool older=false, message_id_t limitId=-1);
  void redraw() {
    updateTimes();
  }

  void setPaused(bool);

  QString getTimeLinePath() const;
  void setTimeLinePath(const QString &value);

signals:
  void message(const QString&);
  void highlightMe();
  void notifyStatus(const QString&);
  void replySignal(const QString&, message_id_t=-1);
  void directMessageSignal(user_id_t user_id);
  void showConversation(int);
  void dofollowUser(int id);
  void dounfollowUser(int id);
  void dodeleteStatus(int id);

public slots:
  void loadOlder(int=-1);

private slots:
  void statusMessage(const QString& msg);
  void onTimelineReady(MessageList);
  void onDirectMessagesReady(MessageList);
  void onReload();                        

protected:
  void keyPressEvent(QKeyEvent* event);

private:

  void updateTimes();
  void cleanTimeLine();
  void cleanLoadButtons();
  MessageWidget* messageWidget(int);

  message_id_t lowestId;
  message_id_t highestId;

  bool highlightNew;
  QString timeLinePath;
  QSocialAPI* socialAPI;
  bool firstLoad;

  QVBoxLayout* itemLayout;
  QWidget* listContainer;

  QMap<message_id_t, QPushButton*> loadButtons;
  QList<QPushButton*> loadButtonsToDelete;
  QSignalMapper* loadSignalMapper;

  QFrame* separator;
  bool isPaused;
  bool useDMButton;
  bool useReverse;
  bool dmTimeLine;
};

#endif
