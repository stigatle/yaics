/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2020 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "yaicstabwidget.h"
#include <QDebug>
//------------------------------------------------------------------------------

YaicsTabWidget::YaicsTabWidget(QWidget* parent) : QTabWidget(parent) {
  sMap = new QSignalMapper(this);
  connect(sMap, SIGNAL(mapped(int)), this, SLOT(highlightTab(int)));
  }
//------------------------------------------------------------------------------

int YaicsTabWidget::addTab(QWidget* page, const QString& label, 
                           const QString& marker, bool highlight) {
  int index = QTabWidget::addTab(page, label);
  if ( marker.isEmpty() )
    tabMarker.insert(label, index);
  else
    tabMarker.insert(marker, index);
  if (highlight)
    addHighlightConnection(page, index);
  return index;
}

//------------------------------------------------------------------------------

int YaicsTabWidget::addTab(QWidget* page, const QIcon& icon,
                           const QString& label, const QString& marker,
                           bool highlight) {
  int index = QTabWidget::addTab(page, icon, label);
  if ( marker.isEmpty() )
    tabMarker.insert(label, index);
  else
    tabMarker.insert(marker, index);
  if (highlight)
    addHighlightConnection(page, index); 

  return index;
}

//------------------------------------------------------------------------------

void YaicsTabWidget::highlightTab(int index) {
  if (index == -1 || index == currentIndex())
    return;
  tabBar()->setTabTextColor(index, Qt::red);
}

//------------------------------------------------------------------------------

void YaicsTabWidget::deHighlightTab(int index) {
  if (index != -1)
  {
    index = currentIndex();
  QPalette pal;
  tabBar()->setTabTextColor(index, pal.color(foregroundRole()));
  }
}

//------------------------------------------------------------------------------

void YaicsTabWidget::addHighlightConnection(QWidget* page, int index) {
  sMap->setMapping(page, index);
  connect(page, SIGNAL(highlightMe()), sMap, SLOT(map()));
}
