/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2020 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "statuslabel.h"

//------------------------------------------------------------------------------

StatusLabel::StatusLabel(QWidget* parent, Qt::WindowFlags f) :
  QLabel(parent, f)
{
  setWordWrap(true);

  setOpenExternalLinks(true);
  setTextInteractionFlags(Qt::TextSelectableByMouse |
                          Qt::LinksAccessibleByMouse);
  setScaledContents(false);
  setLineWidth(2);
  setMargin(0);
  setFocusPolicy(Qt::NoFocus);
}

//------------------------------------------------------------------------------

void StatusLabel::mousePressEvent(QMouseEvent* e) {
  QLabel::mousePressEvent(e);
  e->ignore();
}
