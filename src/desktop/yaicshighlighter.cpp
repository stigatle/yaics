/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2020 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "qsocialapi/status.h"

#include "yaicshighlighter.h"

//------------------------------------------------------------------------------

YaicsHighlighter::YaicsHighlighter(QTextDocument* doc) : QSyntaxHighlighter(doc) 
{
#ifdef USE_ASPELL
  checker = new QASpell(this);
#endif
}

//------------------------------------------------------------------------------

void YaicsHighlighter::highlightBlock(const QString& text) {
  QTextCharFormat statusHighlightFormat;
  statusHighlightFormat.setFontWeight(QFont::Bold);

  QTextCharFormat urlHighlightFormat;
  urlHighlightFormat.setForeground(QBrush(Qt::blue));

  QTextCharFormat spellErrorFormat;
  spellErrorFormat.setFontUnderline(true);
  spellErrorFormat.setUnderlineColor(Qt::red);
  spellErrorFormat.setUnderlineStyle(QTextCharFormat::SpellCheckUnderline);

  QRegExp re("(^|\\s)([!@#])?([\\w']+)");

  int index = text.indexOf(re);
  while (index >= 0) {
    int length = re.matchedLength();
    int offset = re.cap(1).count();
    int s = index+offset;
    int l = length-offset;

    if (!re.cap(2).isEmpty())
      setFormat(s, l, statusHighlightFormat);
#ifdef USE_ASPELL
    else if (!checker->checkWord(re.cap(3)))
      setFormat(s, l, spellErrorFormat);
#endif // USE_ASPELL
    index = text.indexOf(re, index + length);
  }

  QRegExp re2(URL_REGEX);

  index = text.indexOf(re2);
  while (index >= 0) {
    int length = re2.matchedLength();
    setFormat(index, length, urlHighlightFormat);
    index = text.indexOf(re2, index + length);
  }
}
