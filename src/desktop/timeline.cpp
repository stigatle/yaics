/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2020 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "util/util.h"
#include "util/yaicssettings.h"

#include "timeline.h"

//------------------------------------------------------------------------------

TimeLine::TimeLine(const QString& timeLine, QSocialAPI* sa, QWidget* parent,
                   bool highlight, bool dmButton, bool isReverse) :
  QScrollArea(parent), lowestId(-1), highestId(-1), highlightNew(highlight),
  timeLinePath(timeLine), socialAPI(sa), firstLoad(true), separator(NULL),
  isPaused(false), useDMButton(dmButton), useReverse(isReverse)
{
  loadSignalMapper = new QSignalMapper(this);
  connect(loadSignalMapper, SIGNAL(mapped(int)), this, SLOT(loadOlder(int)));
  
  dmTimeLine = timeLinePath.startsWith("direct_messages");

  itemLayout = new QVBoxLayout;
  itemLayout->setSpacing(10);
  itemLayout->addStretch();

 /*
 * We reverse the conversation timeline here by setting the bool to true
 * when we create the timeline.
 */
  if (isReverse)
  {
    itemLayout->setDirection(QBoxLayout::BottomToTop);
  }

  listContainer = new QWidget;
  listContainer->setLayout(itemLayout);

  setWidget(listContainer);
  setWidgetResizable(true);
}

//------------------------------------------------------------------------------

void TimeLine::reload(bool older, message_id_t limitId) {
  if (!older && isPaused)
    return;

  statusMessage("Loading ...");

  QSocialAPIRequest* rq =
    socialAPI->timelineRequest(timeLinePath, limitId, older);
  connect(rq, SIGNAL(timelineReady(MessageList)),
          this, SLOT(onTimelineReady(MessageList)));
  connect(rq, SIGNAL(directMessagesReady(MessageList)),
          this, SLOT(onDirectMessagesReady(MessageList)));

  socialAPI->executeRequest(rq);
}

//------------------------------------------------------------------------------

void TimeLine::updateTimes() {
  for (int i=0; i<itemLayout->count(); i++) {
    MessageWidget* sw = messageWidget(i);
    if (sw)
      sw->updateTime();
  }
}

//------------------------------------------------------------------------------

void TimeLine::setPaused(bool paused) {
  bool oldPaused = isPaused;
  isPaused = paused;

  if (oldPaused == isPaused)
    return;

  if (oldPaused) {
    if (!separator) {
      separator = new QFrame(this);
      separator->setFrameShadow(QFrame::Plain);
      separator->setFrameShape(QFrame::HLine);
    } else {
      itemLayout->removeWidget(separator);
    }
    itemLayout->insertWidget(0, separator);
    separator->show();
    reload();
  }
}

//------------------------------------------------------------------------------

MessageWidget* TimeLine::messageWidget(int i) {
  QLayoutItem* li = itemLayout->itemAt(i);
  if (!li) {
    qDebug() << "TimeLine::messageWidget(" << i << "): "
             << "asking for non-existent item.";
    return NULL;
  }
    
  QLayout* l = li->layout();

  if (!l || l->count()!=2)
    return NULL;

  QLayout* innerL = l->itemAt(0)->layout();

  QWidget* w = innerL->itemAt(1)->widget();
  if (!w)
    return NULL;

  return qobject_cast<MessageWidget*>(w);
}
QString TimeLine::getTimeLinePath() const
{
    return timeLinePath;
}

void TimeLine::setTimeLinePath(const QString &value)
{
    timeLinePath = value;
}


//If we reach the max number of statuses to show at once we clear our
// the timeline and start over by showing the last 20, and then fill up again.
void TimeLine::cleanTimeLine()
{
    YaicsSettings* settings = YaicsSettings::getSettings();

    if (itemLayout->count() > settings->getMaxNumStatuses())
    {
      qDebug() << "Max statuses reached.: " << settings->getMaxNumStatuses();

      qDeleteAll(itemLayout->children());
      delete itemLayout->layout();
      itemLayout = new QVBoxLayout;
      itemLayout->setSpacing(10);
      itemLayout->addStretch();
      qDeleteAll(listContainer->children());
      listContainer->setLayout(itemLayout);

      setWidget(listContainer);
      setWidgetResizable(true);
      statusMessage(tr("Hiding old statuses to clean timeline."));
    }
}

void TimeLine::cleanLoadButtons()
{
    for (int i=0; i<loadButtonsToDelete.size(); i++) {
      QPushButton* pb = loadButtonsToDelete.takeAt(i);
      itemLayout->removeWidget(pb);
      delete pb;
    }
}
//------------------------------------------------------------------------------

void TimeLine::onDirectMessagesReady(MessageList dml) {
    onTimelineReady(dml);
}

//------------------------------------------------------------------------------

void TimeLine::onTimelineReady(MessageList sl) {
  YaicsSettings* settings = YaicsSettings::getSettings();
  QString filter = settings->getTextToFilter();
  int filtered = 0;

  QString messageWord = dmTimeLine ? tr("message") : tr("update");
  QString messageWordP = dmTimeLine ? tr("messages") : tr("updates");

  cleanLoadButtons();

  int i =  itemLayout->count();
  int oldLength = i;
  int newCount = 0;
  message_id_t lastId = -1;
  int lastPos = -1;
  QString newMsg;

  cleanTimeLine();

  // Loop over new statuses to be inserted into timeline
  MessageList::list_t::const_iterator sli = sl.constBegin();
  for (; sli != sl.constEnd(); sli++) {
    Message* msg = sli.value();
    message_id_t id = sli.key();

    // FIXME: this can be done with sl.lowest() etc outside of loop
    if (lowestId == -1 || id < lowestId)
      lowestId = id;
    if (highestId == -1 || id > highestId)
      highestId = id;

    message_id_t wid = -1;
    while (i > 0 && id > wid) {
      // Get id of item just above i in timeline
      MessageWidget* sw = messageWidget(i-1);

      // If this item in the timeline doesn't contain a MessageWidget
      // (i.e. it's a separator or "Load older" button), skip it
      if (!sw) {
        i--;
        continue;
      }

      wid = sw->getId();

      // If id of item to insert is larger than where we are at in the timeline 
      // decrement i (move "up" in timeline, i.e. to newer, higher ids)
      if (id > wid)
        i--;
    }

    // If item already exists in timeline, skip it
    if (id == wid) {
      i--;
      continue;
    }

    // If status contains text to be filtered, skip it
    if (!filter.isEmpty()) {
      QString text = msg->getPlainText();
      if ((filter.startsWith('/') && filter.endsWith('/') &&
           text.contains(QRegExp(filter.mid(1,filter.length()-2)))) ||
          text.contains(filter, Qt::CaseInsensitive)) {
        filtered++;
        continue;
      }
    }

    QString text = msg->getPlainText();
    if ((text.contains(" deleted notice {{",Qt::CaseInsensitive))\
    || (text.contains(" favorited something by ",Qt::CaseInsensitive))\
    || (text.contains(" favourited a status by ",Qt::CaseInsensitive)))
    {
         filtered++;
         continue;
    }

    MessageWidget* sw = new MessageWidget(msg, socialAPI, this);

    sw->setParent(this);

    User* user = sw->getUser();

    if (i != oldLength-1) // don't count when loading older
      newCount++;

    newMsg = user->getScreenName()+" wrote: "+sw->getPlainText();
    if (lastId == -1 || id < lastId)
      lastId = id;

    connect(sw, SIGNAL(linkHovered(const QString&)),
            this, SLOT(statusMessage(const QString&)));
    connect(sw, SIGNAL(replySignal(const QString&, message_id_t)),
            this, SIGNAL(replySignal(const QString&, message_id_t)));
    connect(sw, SIGNAL(directMessageSignal(user_id_t)),
            this, SIGNAL(directMessageSignal(user_id_t)));
    connect(sw, SIGNAL(requestReload()),
            this, SLOT(onReload()));
    connect(sw, SIGNAL(showConversation(int)),
	    this, SIGNAL(showConversation(int)));
    connect(sw,SIGNAL(dofollowUser(int)),
            this,SIGNAL(dofollowUser(int)));
    connect(sw,SIGNAL(dounfollowUser(int)),
            this,SIGNAL(dounfollowUser(int)));
    connect(sw,SIGNAL(dodeleteStatus(int)),
            this,SIGNAL(dodeleteStatus(int)));

    Avatar* avatarLabel = new Avatar(user);

    QHBoxLayout* rowLayout = new QHBoxLayout;
    rowLayout->setSpacing(10);
    rowLayout->addWidget(avatarLabel, 0, Qt::AlignTop);
    rowLayout->addWidget(sw, 0, Qt::AlignTop);

    QVBoxLayout* hrLayout = new QVBoxLayout;
    hrLayout->addLayout(rowLayout);
    hrLayout->addWidget(new QLabel("<hr />"));

    itemLayout->insertLayout(i, hrLayout);

    if (lastPos == -1 || lastPos < i)
      lastPos = i;
    else
      lastPos++;
  } 

  // If there were no items, we still want the "Load older" button to
  // be inserted at the end.
  if (lastPos == -1) {
    lastPos = itemLayout->count()-2;
    lastId = lowestId;
  }
  if ((newCount == RETRIEVE_COUNT-filtered ||
       lastPos+2 == itemLayout->count())
      && !loadButtons.contains(lastId)) {
    QPushButton* pb = new QPushButton(tr("Load older ")+messageWordP, this);
    pb->setFocusPolicy(Qt::NoFocus);

    //If we have a reverse list (like conversation) we force the insert of the
    //'load more' button at the top, this way it'll show at the bottom.
    if (itemLayout->direction() == QBoxLayout::Direction::BottomToTop)
    {
        loadButtons.insert(0, pb);
        itemLayout->insertWidget(0, pb);
    }else
    {
      loadButtons.insert(lastId, pb);
      itemLayout->insertWidget(lastPos+1, pb);
    }


    loadSignalMapper->setMapping(pb, lastId);
    connect(pb, SIGNAL(clicked()), loadSignalMapper, SLOT(map()));
  }

  QString newReport = QString("Received %1 new %2!").arg(newCount).
    arg(newCount>1?messageWordP:messageWord);

  if (highlightNew && newCount && !firstLoad) {
    emit highlightMe();
    QString msg = newMsg;
    if (newCount > 1) 
      msg = newReport;
    emit notifyStatus(msg);
  }

  firstLoad = false;
  statusMessage(tr("Updated"));
}

//------------------------------------------------------------------------------

void TimeLine::statusMessage(const QString& msg) {
  emit message(msg);
}

//------------------------------------------------------------------------------

void TimeLine::loadOlder(int limitId) {
  if (limitId == -1)
    limitId = lowestId;

  QPushButton* pb = loadButtons.take(limitId);

  if (pb) {
    pb->setText(tr("Loading ..."));
    loadButtonsToDelete.append(pb);
    limitId--;
  }

  reload(true, limitId);
}

//------------------------------------------------------------------------------

void TimeLine::onReload() {
  reload();
}

//------------------------------------------------------------------------------

void TimeLine::keyPressEvent(QKeyEvent* event) {
  int key = event->key();

  if (key == Qt::Key_Home || key == Qt::Key_End) {
    bool home = key==Qt::Key_Home;
    QScrollBar* sb = verticalScrollBar();
    sb->setValue(home ? sb->minimum() : sb->maximum());
  } else {
    QScrollArea::keyPressEvent(event);
  }
}
