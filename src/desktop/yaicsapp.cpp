/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2020 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "yaicsapp.h"

#include "util/util.h"

#include "statuswindow.h"
#include "avatar.h"
#include <QNetworkProxy>
#include <QStyleFactory>
//------------------------------------------------------------------------------

QSet<QString> YaicsApp::groups;
QSet<QString> YaicsApp::friends;

//------------------------------------------------------------------------------

YaicsApp::YaicsApp() {
  tempFile = NULL;
  trayIcon = NULL;
  pingReady = false;

  // Read settings.
  settings = YaicsSettings::getSettings(this);
  readSettings();

  settingsDialog = new YaicsSettingsDialog(settings, this);
  connect(settingsDialog, SIGNAL(settingsChanged()),
          this, SLOT(writeSettings()));

  // Remember default Qt style before it is changed.
  defaultTheme = qApp->style()->objectName();
  updateQtStyle();
  updateWindowTitle();
  createActions();
  createMenu();
  setWindowIcon(QIcon(CLIENT_ICON));

  QFont* font = new QFont(settings->getfontName());
  font->setPixelSize(settings->getfontSize());
  font->setItalic(settings->getfontItalic());
  font->setBold(settings->getfontBold());

  qApp->setFont(*font);
#ifdef USE_DBUS
  dbusNotificationInterface =
    new QDBusInterface("org.freedesktop.Notifications",
                       "/org/freedesktop/Notifications",
                       "org.freedesktop.Notifications");
  if (!dbusNotificationInterface->isValid()) {
    qDebug() << "Unable to to connect to org.freedesktop.Notifications "
      "dbus service.";
    dbusNotificationInterface = NULL;
  }
#endif

  updateTrayIcon();

  QNetworkProxy proxy;
  if (settings->getProxyType() == 0) {
    QNetworkProxy::setApplicationProxy(QNetworkProxy::NoProxy);
  } else {
    if (settings->getProxyType() == 1)
        proxy.setType(QNetworkProxy::Socks5Proxy);
    else if (settings->getProxyType() == 2)
        proxy.setType(QNetworkProxy::HttpProxy);

    proxy.setHostName(settings->getProxyIP());
    proxy.setPort(settings->getProxyPort().toInt());
    proxy.setUser(settings->getProxyUsername());
    proxy.setPassword(settings->getProxyPassword());
    QNetworkProxy::setApplicationProxy(proxy);
  }

  // Setup SocialAPI connection
  socialAPI = new QSocialAPI(this);
  socialAPI->setAPIUrl(settings->getAPIUrl());
  socialAPI->setUsername(settings->getUsername());
  socialAPI->setPassword(settings->getPassword());
  socialAPI->setClientSource(CLIENT_FANCY_NAME);
  socialAPI->setIgnoreSSLErrors(settings->getIgnoreSSLWarnings());
  socialAPI->setEnableFancyUrl(settings->getEnableFancyUrl());
  connect(socialAPI, SIGNAL(error(QString)),
          this, SLOT(errorMessage(QString)));

  QSocialAPIRequest* v_rq = socialAPI->verifyCredentials();
  connect(v_rq, SIGNAL(userReady(User*)), 
          this, SLOT(onVerifyCredentials(User*)));
  socialAPI->executeRequest(v_rq);

  // Fetch server config
  QSocialAPIRequest* rq = socialAPI->getServerConfig();
  // connect(rq, SIGNAL(configReady(QSocialAPIConfig*)),
  //         this, SLOT(onConfigReady(QSocialAPIConfig*)));
  socialAPI->executeRequest(rq);

  // Generate main UI
  homeTimeLine = new TimeLine("statuses/home_timeline", socialAPI, this);

  connect(homeTimeLine, SIGNAL(message(const QString&)),
          this, SLOT(statusMessage(const QString&)));
  connect(homeTimeLine, SIGNAL(notifyStatus(const QString&)),
          this, SLOT(notifyMessage(const QString&)));
  connect(homeTimeLine, SIGNAL(replySignal(const QString&, message_id_t)),
          this, SLOT(newNotice(const QString&, message_id_t)));
  connect(homeTimeLine, SIGNAL(showConversation(int)),
	  this, SLOT(addConversationTimeLine(int)));

  if (settings->getEnablePublicTimeline()) {
    // public_and_external_timeline has not been found on many instances.
    QSocialAPIRequest* publicTimeLineRequest = new QSocialAPIRequest(
                "statuses/public_and_external_timeline",
                QStringList(),true,this);
    if(socialAPI->getHttpStatusCode(publicTimeLineRequest) == 404)
      publicTimeLine = new TimeLine("statuses/public_timeline", socialAPI, this);
    else
      publicTimeLine = new TimeLine("statuses/public_and_external_timeline",
                                    socialAPI,this);
    delete publicTimeLineRequest;

    connect(publicTimeLine, SIGNAL(message(const QString&)),
            this, SLOT(statusMessage(const QString&)));
    connect(publicTimeLine, SIGNAL(notifyStatus(const QString&)),
            this, SLOT(notifyMessage(const QString&)));
    connect(publicTimeLine, SIGNAL(replySignal(const QString&, message_id_t)),
            this, SLOT(newNotice(const QString&, message_id_t)));
    connect(publicTimeLine, SIGNAL(showConversation(int)),
	    this, SLOT(addConversationTimeLine(int)));
  }

  mentionsTimeLine = new TimeLine("statuses/mentions", socialAPI, this);
  connect(mentionsTimeLine, SIGNAL(message(const QString&)),
          this, SLOT(statusMessage(const QString&)));
  connect(mentionsTimeLine, SIGNAL(notifyStatus(const QString&)),
          this, SLOT(notifyMessage(const QString&)));
  connect(mentionsTimeLine, SIGNAL(replySignal(const QString&, message_id_t)),
          this, SLOT(newNotice(const QString&, message_id_t)));
  connect(mentionsTimeLine, SIGNAL(showConversation(int)),
	  this, SLOT(addConversationTimeLine(int)));

  userTimeLine = new TimeLine("statuses/user_timeline", socialAPI, this,
                              false, false);
  connect(userTimeLine, SIGNAL(message(const QString&)),
          this, SLOT(statusMessage(const QString&)));
  connect(userTimeLine, SIGNAL(replySignal(const QString&, message_id_t)),
          this, SLOT(newNotice(const QString&, message_id_t)));
  connect(userTimeLine, SIGNAL(showConversation(int)),
	  this, SLOT(addConversationTimeLine(int)));

  tabWidget = new YaicsTabWidget(this);
  connect(tabWidget, SIGNAL(currentChanged(int)),
          this, SLOT(tabSelected(int)));

  connect(tabWidget, SIGNAL(tabCloseRequested(int)), this, SLOT(closeTab(int)));

  if (settings->getEnablePublicTimeline()) {
    tabWidget->addTab(publicTimeLine, tr("&Public"), "Public");
  }


  tabWidget->setTabsClosable(true);

  tabWidget->addTab(homeTimeLine, tr("&Home"), "Home");
  tabWidget->addTab(mentionsTimeLine, tr("&Mentions"), "Mentions");
  tabWidget->addTab(userTimeLine, tr("P&rofile"), "Profile", false);
  setCentralWidget(tabWidget);

   for( int tabIndex = 0; tabIndex < tabWidget->count(); tabIndex = tabIndex + 1 )
   {
       qDebug() << "Removing X icon for tab:" << tabIndex;
       QTabBar *tabBar = tabWidget->findChild<QTabBar *>();
       tabBar->setTabButton(tabIndex, QTabBar::RightSide, 0);
       tabBar->setTabButton(tabIndex, QTabBar::LeftSide, 0);
   }

  timerId = -1;
  reloadTimeline();
  refreshLists();
  resetTimer();

  if (settings->getUsername().isEmpty())
    settingsDialog->exec();
}
//------------------------------------------------------------------------------

void YaicsApp::closeTab(int index) {

    tabWidget->removeTab(index);
    tabWidget->setCurrentIndex(0);
}

void YaicsApp::addConversationTimeLine(int id) {
    QString tmpString = "";
    tmpString = QString::number(id);

    if (!tmpString.isEmpty() || NULL) {
      qDebug() << "addconversationtimeline called. with id:" << tmpString;
      QString conversationTimelinestring= ("statusnet/conversation/" + tmpString);
      qDebug() << "Conversation url:" << conversationTimelinestring;

      //QString conversationTabName = ("Conversation " + tmpString);
      conversationTimeLine = new TimeLine(conversationTimelinestring,socialAPI,this,false,false,true);
      int index = tabWidget->addTab(conversationTimeLine, tr("Conversation"), "Conversation", false);
      tabWidget->setCurrentIndex(index);
      connect(conversationTimeLine, SIGNAL(message(const QString&)),
              this, SLOT(statusMessage(const QString&)));
      connect(conversationTimeLine, SIGNAL(notifyStatus(const QString&)),
              this, SLOT(notifyMessage(const QString&)));
      connect(conversationTimeLine, SIGNAL(replySignal(const QString&, message_id_t)),
              this, SLOT(newNotice(const QString&, message_id_t)));

      conversationTimeLine->reload(false);
      conversationTimeLine->redraw();
    }
}

//------------------------------------------------------------------------------

YaicsApp::~YaicsApp() {
    exitingApplication = true;
    writeSettings();
}

//------------------------------------------------------------------------------

QStringList YaicsApp::getGroups() {
  return QStringList(groups.values());
}

//------------------------------------------------------------------------------

QStringList YaicsApp::getFriends() {
  return QStringList(friends.values());
}

//------------------------------------------------------------------------------

void YaicsApp::showEvent(QShowEvent*) {
  tabWidget->deHighlightTab();
}

//------------------------------------------------------------------------------
void YaicsApp::updateQtStyle() {
  qDebug() << "Using theme" << settings->getQtStyle();
  if (settings->getQtStyle() == "Dark") {
    QFile file(":/images/darkorange.stylesheet");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
#ifdef IS_QT4
    qApp->setStyle(QStyleFactory::create("Plastique"));
#else
    qApp->setStyle(QStyleFactory::create("Fusion"));
#endif
    setStyleSheet(styleSheet);

    QPalette pal(qApp->palette());
    pal.setColor(QPalette::Link, QColor(settings->getlinkColour()));
    pal.setColor(QPalette::LinkVisited, QColor(settings->getlinkColour()));
    qApp->setPalette(pal);
  } else {
    if (QStyleFactory::keys().contains(settings->getQtStyle()))
      qApp->setStyle(QStyleFactory::create(settings->getQtStyle()));
    else {
      qApp->setStyle(defaultTheme);
      qDebug() << "using default:" << defaultTheme;
    }
    setStyleSheet("");
    QPalette pal(qApp->palette());
    pal.setColor(QPalette::Link, QColor(settings->getlinkColour()));
    pal.setColor(QPalette::LinkVisited, QColor(settings->getlinkColour()));
    qApp->setPalette(pal);
  }
}

//------------------------------------------------------------------------------

void YaicsApp::updateWindowTitle() {
  if (!settings->getAPIUrl().isEmpty())
    this->setWindowTitle(QString(CLIENT_FANCY_NAME) + " | " + settings->getAPIUrl());
}

//------------------------------------------------------------------------------

void YaicsApp::updateTrayIcon() {
  useTray = settings->getUseTrayIcon() &&
    QSystemTrayIcon::isSystemTrayAvailable();

  if (useTray) {
    qApp->setQuitOnLastWindowClosed(false);
    if (!trayIcon)
    {
        qDebug() << "Creating tray icon";
        createTrayIcon();
    }
    else
      {
         qDebug() << "Creating tray icon";
         trayIcon->show();
      }
  } else {
    qApp->setQuitOnLastWindowClosed(true);
    if (trayIcon)
      trayIcon->hide();

  }
}

//------------------------------------------------------------------------------

void YaicsApp::notifyMessage(const QString& msg) {
  TimeLine* tl = qobject_cast<TimeLine*>(sender());
  if (!tl)
    return;

  YaicsSettings::notifyType nt = YaicsSettings::DoNothing;
  if (tl == homeTimeLine)
    nt = settings->getHomeNotify();
  else if (tl == mentionsTimeLine)
    nt = settings->getMentionsNotify();

  if (nt == YaicsSettings::DoNothing)
    return;

  if (nt >= YaicsSettings::HighlightTray && useTray)
    trayIcon->setIcon(QIcon(TRAY_ICON_GLOW));
  
  if (nt == YaicsSettings::Popup)
    sendNotification(CLIENT_NAME, msg);
} 

//------------------------------------------------------------------------------

bool YaicsApp::sendNotification(QString summary, QString text) {
#ifdef USE_DBUS
  if (dbusNotificationInterface && dbusNotificationInterface->isValid()) {

    // https://developer.gnome.org/notification-spec/
    QList<QVariant> args;
    args.append(CLIENT_NAME);   // Application Name
    args.append(0123U);         // Replaces ID (0U)
    args.append(QString());     // Notification Icon
    args.append(summary);       // Summary
    args.append(text);          // Body
    args.append(QStringList()); // Actions

    QVariantMap hints;
    // for hints to make icon, see
    // https://dev.visucore.com/bitcoin/doxygen/notificator_8cpp_source.html
    args.append(hints);
    args.append(1000);

    dbusNotificationInterface->callWithArgumentList(QDBus::NoBlock,
                                                    "Notify", args);
    return true;
  }
#endif

  if (QSystemTrayIcon::supportsMessages()) {
    trayIcon->showMessage(CLIENT_NAME, summary+" "+text);
    return true;
  }
  
  qDebug() << "[NOTIFY]" << summary << text;
  return false;
}

//------------------------------------------------------------------------------

void YaicsApp::resetNotifications() {
  if (useTray)
    trayIcon->setIcon(QIcon(TRAY_ICON));
  tabWidget->deHighlightTab();
}

//------------------------------------------------------------------------------

void YaicsApp::tabSelected(int index) {
  tabWidget->deHighlightTab(index);
  resetNotifications();
}

//------------------------------------------------------------------------------

void YaicsApp::timerEvent(QTimerEvent* event) {
  if (event->timerId() != timerId)
    return;
  reloadTimeline();
}

//------------------------------------------------------------------------------

void YaicsApp::resetTimer() {
  if (timerId != -1)
    killTimer(timerId);
  timerId = startTimer(settings->getReloadTime()*60*1000);
}

//------------------------------------------------------------------------------

void YaicsApp::createActions() {
  exitAct = new QAction(tr("E&xit"), this);
  exitAct->setShortcut(QString("Ctrl+Q"));
  connect(exitAct, SIGNAL(triggered()), this, SLOT(exit()));

  prefsAct = new QAction(tr("Preferences"), this);
  // prefsAct->setShortcut(QString("Ctrl+P"));
  connect(prefsAct, SIGNAL(triggered()), this, SLOT(preferences()));

  reloadAct = new QAction(tr("&Reload timeline"), this);
  reloadAct->setShortcut(QString("Ctrl+R"));
  connect(reloadAct, SIGNAL(triggered()), this, SLOT(manualReloadTimeline()));

  loadOlderAct = new QAction(tr("Load &older in timeline"), this);
  loadOlderAct->setShortcut(QString("Ctrl+O"));
  connect(loadOlderAct, SIGNAL(triggered()), this, SLOT(loadOlder()));

  pauseAct = new QAction(tr("&Pause home timeline"), this);
  pauseAct->setShortcut(QString("Ctrl+P"));
  pauseAct->setCheckable(true);
  connect(pauseAct, SIGNAL(triggered()), this, SLOT(pauseTimeline()));

  aboutAct = new QAction(tr("&About"), this);
  connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

  bugtrackerAct = new QAction(tr("&Bugtracker"),this);
  connect(bugtrackerAct,SIGNAL(triggered()),this,SLOT(openbugtracker()));

  aboutQtAct = new QAction("About &Qt", this);
  connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

  newNoticeAct = new QAction(tr("&New notice"), this);
  newNoticeAct->setShortcut(QString("Ctrl+N"));
  connect(newNoticeAct, SIGNAL(triggered()), this, SLOT(newNotice()));

}

//------------------------------------------------------------------------------

void YaicsApp::createMenu() {
  fileMenu = new QMenu("&Yaics", this);
  fileMenu->addAction(newNoticeAct);
  fileMenu->addSeparator();
  fileMenu->addAction(reloadAct);
  fileMenu->addAction(loadOlderAct);
  fileMenu->addAction(pauseAct);
  fileMenu->addSeparator();
  fileMenu->addAction(prefsAct);
  fileMenu->addSeparator();
  fileMenu->addAction(exitAct);
  menuBar()->addMenu(fileMenu);

  helpMenu = new QMenu(tr("&Help"), this);
  helpMenu->addAction(aboutAct);
  helpMenu->addAction(aboutQtAct);
  helpMenu->addAction(bugtrackerAct);
  menuBar()->addMenu(helpMenu);
}

//------------------------------------------------------------------------------

void YaicsApp::createTrayIcon() {
  trayIconMenu = new QMenu(this);
  trayIconMenu->addAction(newNoticeAct);
  trayIconMenu->addAction(prefsAct);
  trayIconMenu->addSeparator();
  trayIconMenu->addAction(exitAct);
  
  trayIcon = new QSystemTrayIcon(QIcon(TRAY_ICON));
  connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
          this, SLOT(trayIconActivated(QSystemTrayIcon::ActivationReason)));
  trayIcon->setContextMenu(trayIconMenu);
  trayIcon->setToolTip(CLIENT_FANCY_NAME);
  trayIcon->show();
}

//------------------------------------------------------------------------------

void YaicsApp::preferences() {
  settingsDialog->exec();
}

//------------------------------------------------------------------------------

void YaicsApp::exit() {
  useTray = settings->getUseTrayIcon() &&
    QSystemTrayIcon::isSystemTrayAvailable();
    if (useTray)
    {
        if (trayIcon)
        {
            trayIcon->deleteLater();
        }
    }
  qApp->exit();
}

void YaicsApp::openbugtracker()
{
    QDesktopServices::openUrl(QUrl(BUGTRACKER_URL));
}
//------------------------------------------------------------------------------

void YaicsApp::about() { 
  static const QString GPL = 
    tr("<p>Yaics is free software: you can redistribute it and/or modify it "
    "under the terms of the GNU General Public License as published by "
    "the Free Software Foundation, either version 3 of the License, or "
    "(at your option) any later version.</p>"
    "<p>Yaics is distributed in the hope that it will be useful, but "
    "WITHOUT ANY WARRANTY; without even the implied warranty of "
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU "
    "General Public License for more details.</p>"
    "<p>You should have received a copy of the GNU General Public License "
    "along with Yaics.  If not, see "
    "<a href=\"http://www.gnu.org/licenses/\">http://www.gnu.org/licenses/</a>."
       "</p>");
  
  QString mainText = 
    QString("<p><b>%1 %2</b> - %3<br/><a href=\"%4\">%4</a><br/>"
            + tr("Copyright &copy; 2010-2015 Mats Sj&ouml;berg")
            + " - <a href=\"https://social.mayfirst.org/sazius\">sazius@social.mayfirst.org</a>,"
            "<br />"
            + tr("Copyright &copy; 2014-2020 Stig Atle Steffensen")
            + " - <a href=\"https://gnusocial.no/index.php/stigatle\">stigatle@gnusocial.no</a>."
            "</p>"
            + tr("<p>Report bugs and feature requests at ")
            +   "<a href=\"%5\">%5</a>.</p>")
    .arg(CLIENT_FANCY_NAME)
    .arg(CLIENT_VERSION)
    .arg(tr("A simple Qt-based GNU social client."))
    .arg(WEBSITE_URL)
    .arg(BUGTRACKER_URL);
  
  QMessageBox::about(this, QString(tr("About %1")).arg(CLIENT_FANCY_NAME),
          mainText + GPL);
}

//------------------------------------------------------------------------------

void YaicsApp::newNotice(const QString& msg, message_id_t id) {
  StatusWindow* w = new StatusWindow(socialAPI, msg, id, this);
  connect(w, SIGNAL(messageSent()), this, SLOT(reloadTimeline()));
  w->show();
}

//------------------------------------------------------------------------------

void YaicsApp::writeSettings() {
  QSettings s(CLIENT_NAME, CLIENT_NAME);
  
  s.beginGroup("MainWindow");
  s.setValue("size", size());
  s.setValue("pos", pos());
  s.endGroup();

  settings->write(s);

  // reload stuff that might have been changed ...
  socialAPI->setAPIUrl(settings->getAPIUrl());
  socialAPI->setUsername(settings->getUsername());
  socialAPI->setPassword(settings->getPassword());
  socialAPI->setIgnoreSSLErrors(settings->getIgnoreSSLWarnings());
  socialAPI->setEnableFancyUrl(settings->getEnableFancyUrl());
  updateQtStyle();
  updateWindowTitle();
  if (exitingApplication != true)
  {
    updateTrayIcon();
  }else
  {
      qDebug() << "Exiting yaics, not updating system icon.";
  }
  reloadTimeline();
  resetTimer();
}

//------------------------------------------------------------------------------

void YaicsApp::readSettings() {
  QSettings s(CLIENT_NAME, CLIENT_NAME);

  s.beginGroup("MainWindow");
  resize(s.value("size", QSize(550, 500)).toSize());
  move(s.value("pos", QPoint(0, 0)).toPoint());
  s.endGroup();

  settings->read(s);
}


//------------------------------------------------------------------------------

void YaicsApp::statusMessage(const QString& msg) {
  statusBar()->showMessage(msg);
}

//------------------------------------------------------------------------------

void YaicsApp::errorMessage(const QString& msg) {
  // QMessageBox msgBox;
  // msgBox.setText(msg);
  // msgBox.setIcon(QMessageBox::Warning);
  // msgBox.exec();

  statusMessage("Error: "+msg);
  qDebug() << "[ERROR]" << msg;
}

//------------------------------------------------------------------------------

void YaicsApp::trayIconActivated(QSystemTrayIcon::ActivationReason reason) {
  if (reason == QSystemTrayIcon::Trigger) {
    trayIcon->setIcon(QIcon(TRAY_ICON));
    setVisible(!isVisible());
    if (isVisible())
      activateWindow();
  }
}

//------------------------------------------------------------------------------

void YaicsApp::manualReloadTimeline() {
  reloadTimeline(true);
}

//------------------------------------------------------------------------------

void YaicsApp::reloadTimeline(bool manual) {
  if (settings->getUsername().isEmpty())
    return;

  homeTimeLine->reload(false);
  mentionsTimeLine->reload(false);
  userTimeLine->reload(false);
  if (settings->getEnablePublicTimeline())
  {
      if (publicTimeLine != NULL)
      {
          publicTimeLine->reload(false);
      }
  }

  //conversationTimeLine->reload(false);
  // refresh list of groups
  if (manual) {
    refreshLists();

    //Reset the timer if we manually refresh, so that it does not
    //automatically refresh before the timer's time has been hit again.
    resetTimer();
  }
}

//------------------------------------------------------------------------------

void YaicsApp::redrawTimeline() {
  if (settings->getUsername().isEmpty())
    return;

  homeTimeLine->redraw();
  mentionsTimeLine->redraw();
  userTimeLine->redraw();
 // conversationTimeLine->redraw();
  if (settings->getEnablePublicTimeline())
    publicTimeLine->redraw();
}  

//------------------------------------------------------------------------------

void YaicsApp::pauseTimeline() {
    bool pause = pauseAct->isChecked();

    if (pause)
    {
        homeTimeLine->setPaused(pause);
        if (settings->getEnablePublicTimeline())
        {
            publicTimeLine->setPaused(pause);
        }
        statusMessage(tr("Timelines are paused."));
    } else
        statusMessage(tr("Timelines are resumed."));
}

//------------------------------------------------------------------------------

void YaicsApp::refreshLists() {
  QSocialAPIRequest* frq = socialAPI->friends();
  connect(frq,  SIGNAL(friendsReady(UserList)),
            this, SLOT(onFriendsReady(UserList)));
  socialAPI->executeRequest(frq);

  if (pingReady && !socialAPI->isFriendica()) {
    QSocialAPIRequest* grq = socialAPI->groups();
    connect(grq, SIGNAL(groupsReady(QStringList)),
            this, SLOT(onGroupsReady(QStringList)));
    socialAPI->executeRequest(grq);
  }
}

//------------------------------------------------------------------------------

void YaicsApp::onFriendsReady(UserList list) {
  UserList::const_iterator it = list.begin();
  for (; it != list.end(); it++)
    friends.insert((*it)->getScreenName());
}

//------------------------------------------------------------------------------

void YaicsApp::onGroupsReady(QStringList list) {
  QStringList::const_iterator it = list.begin();
  for (; it != list.end(); it++)
    groups.insert(*it);
}

//------------------------------------------------------------------------------

void YaicsApp::loadOlder() {
  TimeLine* tl = qobject_cast<TimeLine*>(tabWidget->currentWidget());
  if (tl)
    tl->loadOlder();
}

//------------------------------------------------------------------------------

void YaicsApp::onVerifyCredentials(User* /*user*/) {
  QSocialAPIRequest* rq = socialAPI->friendicaPing();
  connect(rq, SIGNAL(pingReady()), this, SLOT(onPingReady()));
  socialAPI->executeRequest(rq);
}

//------------------------------------------------------------------------------

void YaicsApp::onPingReady() {
  pingReady = true;
  qDebug() << "[PING]" << (socialAPI->isFriendica() ?
                           "Friendica detected." :
                           "Not found, assuming GNU social.");
  redrawTimeline();
}
