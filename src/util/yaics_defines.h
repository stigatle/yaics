/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2020 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef YAICS_DEFINES_H
#define YAICS_DEFINES_H

#define CLIENT_NAME           "yaics"
#define CLIENT_FANCY_NAME     "Yaics"
#define CLIENT_VERSION        "0.7-1"
#define CLIENT_URL            "https://gitlab.com/stigatle/yaics/"

#define CLIENT_ICON           ":/images/yaics.svg"
#define TRAY_ICON             ":/images/tray.png"
#define TRAY_ICON_GLOW        ":/images/tray-glow.png"
#define WEBSITE_URL           "https://gitlab.com/stigatle/yaics"
#define BUGTRACKER_URL        "https://gitlab.com/stigatle/yaics/issues"

#endif
