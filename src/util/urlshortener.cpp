/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2020 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "urlshortener.h"
#include "util.h"

//------------------------------------------------------------------------------

QMap<QString,QString> UrlShortener::m_shortUrl;

//------------------------------------------------------------------------------

UrlShortener::UrlShortener(const QString& url, QObject* parent) :
  QObject(parent), m_url(url)
{
  m_nam = new QNetworkAccessManager(this);
  connect(m_nam, SIGNAL(finished(QNetworkReply*)),
          this, SLOT(onFinished(QNetworkReply*)));
}

//------------------------------------------------------------------------------

QString UrlShortener::shortUrl() {
  if (m_shortUrl.contains(m_url)) {
    const QString& surl = m_shortUrl.value(m_url);
    if (surl.isEmpty())
      return m_url;
    else
      return surl;
  }

  // QString urlShortenerUrl = "http://b1t.it";
  // QByteArray data = QUrl::toPercentEncoding("url="+m_url,"=");
  QString urlShortenerUrl = "http://ur1.ca";
  QByteArray data = QUrl::toPercentEncoding("longurl="+m_url,"=");
  m_nam->post(QNetworkRequest(QUrl(urlShortenerUrl)), data);
  m_shortUrl.insert(m_url, "");

  return m_url;
}

//------------------------------------------------------------------------------

void UrlShortener::onFinished(QNetworkReply* nr) {
  if (nr->error()) {
    emit error("Network error: "+nr->errorString());
    return;
  }

  QString output = nr->readAll();

  QRegExp rx("Your ur1 is: <a href=\"([^\"]+)\"");
  int pos = rx.indexIn(output);

  if (pos != -1) {
    QString shortUrl = rx.cap(1);
    m_shortUrl.insert(m_url, shortUrl);
    emit urlReady(m_url, shortUrl);
  }
  
  nr->deleteLater();
}
