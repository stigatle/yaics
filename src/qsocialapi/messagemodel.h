/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2020 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _MESSAGEMODEL_H_
#define _MESSAGEMODEL_H_

#include <QAbstractListModel>
#include <QObject>

#include "qsocialapi.h"
#include "message.h"
#include "messagelist.h"

//------------------------------------------------------------------------------

class MessageModel : public QAbstractListModel {
  Q_OBJECT
  Q_PROPERTY(bool loading
             READ loading
             NOTIFY loadingChanged)

public:
  enum MessageRoles {
    IdRole = Qt::UserRole+1,
    UserIdRole,
    UserRole,
    UserFullRole,
    DateRole,
    SourceRole,
    TextRole,
    PathRole,
    RepeatRole,
    InReplyToRole,
    ImageFileNameRole,
    FavouritedRole,
    LastMessageRole // dummy role, leave this as last
  };

  MessageModel(QSocialAPI* os, QObject* parent=0, bool oldestFirst=false);

  void reset(QSocialAPI* os);

  message_id_t oldestId() const;

  void loadRequest(QSocialAPIRequest* req);

  bool loading() const { return m_loading; }

  void clear();
  
  int insert(Message* msg, int i=-1);
  
  void insert(MessageList ml);

  bool update(Message* msg);

  void debugDump() const;
  
  int rowCount(const QModelIndex& parent = QModelIndex()) const;

  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;

  void setFilter(const QString& f) { m_filter = f; }

  virtual QHash<int, QByteArray> roleNames() const { return m_roles; }

signals:
  void loadingChanged();

private slots:
  void onTimelineReady(MessageList);

protected:
  QString repeatUser(const Message* msg) const;
  
  void setLoading(bool loading);

  QList<Message*> m_list;

  bool m_loading;

  QSocialAPI* m_sn;

  bool m_oldestFirst;

  QString m_filter;

  QHash<int, QByteArray> m_roles;
};

#endif
