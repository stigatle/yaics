/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2020 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "qsocialapirequest.h"

//------------------------------------------------------------------------------

QSocialAPIRequest::QSocialAPIRequest(QObject* parent) : QObject(parent),
                                                        post(false) {}

//------------------------------------------------------------------------------

QSocialAPIRequest::QSocialAPIRequest(QString resource,
                                     QStringList params, bool doPost,
                                     QObject* parent) :
  QObject(parent), post(doPost), origResource(resource), mSilentError(false) {
  
  bool debug = false;

  if (debug)
    qDebug() << "QSocialAPIRequest(" << resource << params << ")";

  requestPath = resource.toUtf8()+".xml";

  for (int i=0; i<params.size(); i++)
    data += (i?"&":"")+QUrl::toPercentEncoding(params.at(i),"=");

  if (!post && !data.isEmpty())
    requestPath += "?"+data;
}

//------------------------------------------------------------------------------

void QSocialAPIRequest::setPing() {
  emit pingReady();
}

//------------------------------------------------------------------------------

void QSocialAPIRequest::setOK() {
  emit OKReady();
}

//------------------------------------------------------------------------------

void QSocialAPIRequest::setTimeline(MessageList list) {
  emit timelineReady(list);
}

//------------------------------------------------------------------------------

void QSocialAPIRequest::setStatus(StatusMessage* status) {
  emit statusReady(status);
}

//------------------------------------------------------------------------------

void QSocialAPIRequest::setFriends(UserList list) {
  emit friendsReady(list);
}

//------------------------------------------------------------------------------

void QSocialAPIRequest::setGroups(QStringList list) {
  emit groupsReady(list);
}

//------------------------------------------------------------------------------

void QSocialAPIRequest::setUser(User* user) {
  emit userReady(user);
}

//------------------------------------------------------------------------------

void QSocialAPIRequest::setConfig(QSocialAPIConfig* c) {
  emit configReady(c);
}
