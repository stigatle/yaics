/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2020 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "user.h"

#include "util/util.h"

//------------------------------------------------------------------------------

User::User(const QDomNode& node, QObject* parent) : QObject(parent) {
  setFromXML(node);
}

//------------------------------------------------------------------------------

void User::setFromXML(const QDomNode& node) {
  id = getXMLElementText(node, "id").toLong();
  fullName = getXMLElementText(node, "name");
  screenName = getXMLElementText(node, "screen_name");

  url = getXMLElementText(node, "statusnet:profile_url");
  avatarUrl = getXMLElementText(node, "profile_image_url");
  isfollowing = getXMLElementText(node,"following");
  urlDomain = urlToDomain(url);

  /* Fields not used yet
     <location>City, Country e.g.</location>
     <description></description>
     <url></url>
     <protected>false</protected>
     <followers_count></followers_count>
     <profile_background_color>#AABBCC</profile_background_color>
     <profile_text_color></profile_text_color>
     <profile_link_color>#AABBCC</profile_link_color>
     <profile_sidebar_fill_color>#AABBCC</profile_sidebar_fill_color>
     <profile_sidebar_border_color></profile_sidebar_border_color>
     <friends_count>123</friends_count>
     <created_at>Wed Jul 02 17:33:29 +0000 2008</created_at>
     <favourites_count>32</favourites_count>
     <utc_offset>-21600</utc_offset>
     <time_zone>City/County</time_zone>
     <profile_background_image_url></profile_background_image_url>
     <profile_background_tile>false</profile_background_tile>
     <statuses_count>123</statuses_count>
     <following>true</following>
     <statusnet:blocking>false</statusnet:blocking>
     <notifications>true</notifications>
  */
}

//------------------------------------------------------------------------------

void User::update(const QDomNode& node) {
  QString newAvatarUrl = getXMLElementText(node, "profile_image_url");
  if (newAvatarUrl != avatarUrl) {
    avatarUrl = newAvatarUrl;
    emit avatarChanged();
  }    
}
