Yaics
=====

*Yet Another Interaction Client Something.*

A simple [GNU social][1] client developed in [Qt][]. My intent has
been to keep the user interface relatively clean and simple, so it
only supports a single account and doesn't have many bells and
whistles, but it works fine for me in everyday use.

## Packages

Arch Linux:
[https://aur.archlinux.org/packages/yaics-git]

Debian/Ubuntu:
[http://stigatle.no/yaics/]

openSUSE:
[https://build.opensuse.org/package/show/network/yaics]

Windows:
[http://stigatle.no/yaics/]

## Obligatory screenshot

![](images/screenshot.png)

As you can see, Qt looks and runs nicely in Gtk environments
(screenshot was made in MATE).

## Source code

The newest source code is available from <https://gitlab.com/stigatle/yaics/>,
and can be obtained e.g. via the command line like this:

    git clone https://gitlab.com/stigatle/yaics.git

All of the source code, images and documentation are licensed under the
[GPLv3+][GPL].

[1]:         http://gnu.io/social
[Qt]:        http://qt.io/
[GPL]:       https://gnu.org/licenses/gpl.html
[https://aur.archlinux.org/packages/yaics/]:      https://aur.archlinux.org/packages/yaics/
[https://build.opensuse.org/package/show/network/yaics]:      https://build.opensuse.org/package/show/network/yaics
[http://stigatle.no/yaics/]:       http://stigatle.no/yaics/

## Requirements

Requires Qt 4.8 or higher.
Installing the following packages with your favourite package
manager on a recent Debian-based distro should get everything you
need to compile:

    apt-get install qt5-default build-essential libqt5webkit5-dev libaspell-dev

On Fedora 32 you need:

    dnf group install   "Development Tools" &&
    dnf install   aspell-devel qt5-devel qt5-qtwebkit-devel

On Windows you need the openssl dll's.
You can download and install from:
https://slproweb.com/download/Win64OpenSSL_Light-1_1_1i.exe
Then copy the dll's from bin into where the yaics.exe is located.
The released version has this alreay, 
so this is only needed when compiling from source.

## Installation

To compile the main source code for the desktop client go to the
directory `build/desktop`. If you wish to use the Qt Creator IDE,
just open the yaics.pro projects and compile it from there.
Alternatively (if you are old school like me) type the following in
the command line in the same directory:

    $ qmake-qt5 && make

This should produce the "yaics" executable. If you to generate a
TAGS file for use with [Emacs][]:

    $ qmake-qt5 CONFIG+=etags

Other useful CONFIG parameter is "debug" which generates debug info
for use with [gdb][].

To generate documentation type "make doc". This requires that you
have [Markdown][2] installed. At the moment this just generates HTML
versions of Markdown-formatted text files.

[2]:     http://daringfireball.net/projects/markdown/
[gdb]:   https://gnu.org/software/gdb/
[Emacs]: https://gnu.org/software/emacs/

## Usage

On first use you need to enter your account information, you should
be automatically shown the Account tab of the Preferences (or go to
Yaics → Preferences via the menu bar). By default the GNU social
Instance is set to https://gnusocial.no/. You can change this to any
other GNU social instance that you might want to connect to. When
you close the Preferences window the time lines should be
automatically updated.

In the main window you can see three tabs representing five
different time lines: *home*, *mentions*, *me*, *inbox* and
*outbox*. You can switch tabs by clicking on them or pressing
*Ctrl-TAB* on the keyboard. The *home* time line is the one showing
status updates from you and your friends and from any groups that
you have joined. The *mentions* time line shows status updates
mentioning your screen name. The *me* time line shows all status
updates made by you. The *inbox* and *outbox* are the received
respectively sent direct messages.

By pressing *Ctrl-R* (or selecting the equivalent menu item) all
time lines will be reloaded. The time lines are also automatically
reloaded at an interval that can be configured in Preferences.

When you go to the bottom of a time line there is a button for
loading older status updates. Pressing *Ctrl-O* will do the same
thing as well. Furthermore, if there are more new status updates
than 20 a "load more" button will appear in the middle of the time
line as well. This typically happens if your client is disconnected
for a long time, e.g. keeping the computer suspended for a few
hours without closing yaics.

When you wish to read your backlog of status updates it can
sometimes be annoying when the timeline updates itself
automatically and you loose your place. The "pause" mode – inspired
by the great curses-based GNU social client [identicurse][] –
attempts to address this problem. By pressing *Ctrl-P* you can
activate the pause mode which disables loading of newer status
updates. When you press *Ctrl-P* again pause mode is disabled and
the timeline is reloaded. A line is drawn between status updates
indicating where you started pause mode. Above that line are new
unread items. Pause mode can also be activated and deactivated from
the Yaics → Pause home timeline menu item.

To write a new status update chose Yaics → New status from the menu
(or press *Ctrl-N*. A new window will open where you can type your
message. Below the text editing field a character counter is
presented. To send press the Send button or press *Ctrl-Return* on
the keyboard. Similarly a new direct message can be written by
Yaics → New direct message (or *Ctrl-D*). The editing window is
otherwise similar but has a pull down of possible recipients.

Each status update has a button for favouriting, repeating, direct
messaging the sender or making a normal reply to that message.
Pressing either the direct message or reply button will open a new
message window prefilled with the appropriate information. (When
repeating !groups will be automatically converted into #tags to
prevent accidental group "spamming".)

If you close the main window the program will remain running and
can be accessed via an icon in the notification area (system tray).
While hidden the program can notify you of new status updates or
mentions of your screen name, either by changing the appearance of
the icon or by a notification text. The exact kind of notification
for different circumstances can be configured via Preferences.

[identicurse]: https://github.com/identicurse/IdentiCurse

## Missing features

- No url shortening support. (It does however expand some urls
  shortened by others.)


## License

Copyright 2010-2015 Mats Sjoberg <mats@sjoberg.fi>,

Copyright 2014-2021 Stig Atle Steffensen <stigatle@cryptolab.net>.

All of the source code, images and documentation are licensed under
GPLv3+.

*GPLv3+*

Yaics is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your
option) any later version.

Yaics is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Yaics. If not, see <http://www.gnu.org/licenses/>.
