%define version 0.7-1
%define release 1
%define name yaics
%define reponame yaics

Summary: GNU Social client
Name: %{name}
Version: 0.7
Release: 1
License: GPLv3+
Prefix: /usr/bin
URL: https://gitlab.com/stigatle/yaics/
Source0: https://gitlab.com/stigatle/yaics/-/archive/0.7-1/yaics-0.7-1.tar.gz
BuildRequires: aspell-devel
BuildRequires: qt5-devel
BuildRequires: qt5-qtwebkit-devel

Requires: aspell
Requires: qt5
Requires: qt5-qtwebkit

%description
Yaics is a simple Qt-based desktop client for GNU social, a
distributed social networking system.

%global debug_package %{nil}

%prep
%setup -q -n %{name}-0.7-1

%build
qmake-qt5 CONFIG+=%{_arch} yaics.pro PREFIX=$RPM_BUILD_ROOT%{_prefix}
make

%install
rm -rf ${buildroot}
mkdir %{buildroot}%{prefix} -p
%make_install

%clean
rm -rf ${buildroot}

%files
%license COPYING
%doc AUTHORS ChangeLog README.md
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/*.png
%dir %{_datadir}/appdata/
%{_datadir}/appdata/%{name}.appdata.xml


%changelog
* Tuesday Feb 2 2021 Stig Atle Steffensen  <StigAtle@cryptolab.net> - 0.7-1
------------------------------------------------------------------------------------------
Version 0.7-1
------------------------------------------------------------------------------------------
* Merge branch 'bkmgit/yaics-master'
* add spec file for Fedora
* Fixed bug that caused cached images not to load properly. They are downloaded if it's not found on disk.
* Updated dates in copyright notice in readme.md
* Updated dates in cpp\h files for copyright notice..
* Added a missed checkbox for 'enable fancy urls' that should have been in previous commit.
* Updated readme with openssl information for windows.
* Added 'fancy url' setting to set what the server has. Updated default url to gnusocial.no. Updated 'about' page.
* Status message editor size \ position is now saved and restored.
* Added 'delete' button for your own statuses. Added filter for various information messages that does not show up by default in qvitter web page.
* Deprecated DM feature. Changed deafult url to quitter.no instead of .se
* Added error message when id from server is wrong. Stripped down the time string to show hr\min.


