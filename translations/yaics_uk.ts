<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>DirectMessageWindow</name>
    <message>
        <source>Write a direct message to </source>
        <translation>Написати приватне повідомлення </translation>
    </message>
    <message>
        <source>Send direct message</source>
        <translation>Надіслати приватне повідомлення</translation>
    </message>
</context>
<context>
    <name>MessageWidget</name>
    <message>
        <source>reply</source>
        <translation>відповісти</translation>
    </message>
    <message>
        <source>direct</source>
        <translation>ПП</translation>
    </message>
    <message>
        <source>conversation</source>
        <translation>балачка</translation>
    </message>
    <message>
        <source> from </source>
        <translation> від </translation>
    </message>
    <message>
        <source>in context</source>
        <translation>в контексті</translation>
    </message>
    <message>
        <source>in reply to </source>
        <translation>у відповідь на </translation>
    </message>
    <message>
        <source>unknown</source>
        <translation>невідомий</translation>
    </message>
    <message>
        <source>[repeated by %1]</source>
        <translation>[%1 повторив]</translation>
    </message>
    <message>
        <source>Mark notice as favourite</source>
        <translation>Додати запис до вибраного</translation>
    </message>
    <message>
        <source>Repeat notice</source>
        <translation>Повторити допис</translation>
    </message>
    <message>
        <source>Reply to notice</source>
        <translation>Відповісти на допис</translation>
    </message>
    <message>
        <source>Send direct message</source>
        <translation>Надіслати приватне повідомлення</translation>
    </message>
    <message>
        <source>Open conversation tab</source>
        <translation>Відкрити вкладку з балачкою</translation>
    </message>
</context>
<context>
    <name>MessageWindow</name>
    <message>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <source>Send message</source>
        <translation>Надіслати повідомлення</translation>
    </message>
    <message>
        <source>Sending ...</source>
        <translation>Надсилаю ...</translation>
    </message>
</context>
<context>
    <name>Prefs</name>
    <message>
        <source>Preferences</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <source>&amp;Main</source>
        <translation>&amp;Основні</translation>
    </message>
    <message>
        <source>Update interval (in minutes):  </source>
        <translation>Інтервал оновлень (у хвилинах):  </translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Сповіщення</translation>
    </message>
    <message>
        <source>Home:</source>
        <translation>Головна:</translation>
    </message>
    <message>
        <source>Mentions:</source>
        <translation>Згадки:</translation>
    </message>
    <message>
        <source>Do nothing</source>
        <translation>Нічого не робити</translation>
    </message>
    <message>
        <source>Highlight tray icon</source>
        <translation>Підсвітити значок у вигульку</translation>
    </message>
    <message>
        <source>Show linked/attached images in notices.</source>
        <translation>Підсвітити пов&apos;язані/вкладені зображення.</translation>
    </message>
    <message>
        <source>Popup notification</source>
        <translation>Спливаюче повідомлення</translation>
    </message>
    <message>
        <source>Filtering</source>
        <translation>Фільтрація</translation>
    </message>
    <message>
        <source>Text to filter:</source>
        <translation>Текст для фільтрації:</translation>
    </message>
    <message>
        <source>Enclose in /../ for regular expression.</source>
        <translation>Обрамити у /../ для регулярного виразу.</translation>
    </message>
    <message>
        <source>Prepend replies with @username.</source>
        <translation>Починати відповіді з @username.</translation>
    </message>
    <message>
        <source>Show icon in system tray.</source>
        <translation>Показувати значок у вигульку.</translation>
    </message>
    <message>
        <source>Select theme:</source>
        <translation>Обрати тему:</translation>
    </message>
    <message>
        <source>Set link colour</source>
        <translation>Задати власний колір посилань</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>За замовчуванням</translation>
    </message>
    <message>
        <source>Enable &apos;Public&apos; tab.</source>
        <translation>Відображати вкладку &apos;Загальне&apos;.</translation>
    </message>
    <message>
        <source>A&amp;ccount</source>
        <translation>О&amp;бліковий запис</translation>
    </message>
    <message>
        <source>Username:</source>
        <translation>Ім&apos;я користувача:</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <source>Ignore SSL warnings</source>
        <translation>Ігнорувати попередження SSL</translation>
    </message>
    <message>
        <source>P&amp;roxy</source>
        <translation>П&amp;роксі</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Відсутня</translation>
    </message>
    <message>
        <source>Proxy type:</source>
        <translation>Тип проксі:</translation>
    </message>
    <message>
        <source>IP:</source>
        <translation>IP:</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Порт:</translation>
    </message>
    <message>
        <source>Socks5</source>
        <translation></translation>
    </message>
    <message>
        <source>HTTP</source>
        <translation></translation>
    </message>
    <message>
        <source>GNU social Instance:</source>
        <translation>Інстанс GNU social:</translation>
    </message>
</context>
<context>
    <name>StatusWindow</name>
    <message>
        <source>Write a reply to %1</source>
        <translation>Відповісти %1</translation>
    </message>
    <message>
        <source>Send notice</source>
        <translation>Надіслати допис</translation>
    </message>
    <message>
        <source>Write a new notice</source>
        <translation>Написати новий допис</translation>
    </message>
</context>
<context>
    <name>TimeLine</name>
    <message>
        <source>Updated</source>
        <translation>Оновлено</translation>
    </message>
    <message>
        <source>Loading ...</source>
        <translation>Завантажую ...</translation>
    </message>
    <message>
        <source>Load older </source>
        <translation>Завантажити старіші </translation>
    </message>
    <message>
        <source>message</source>
        <translation>повідомлення</translation>
    </message>
    <message>
        <source>update</source>
        <translation>оновлення</translation>
    </message>
    <message>
        <source>messages</source>
        <translation>повідомлення</translation>
    </message>
    <message>
        <source>updates</source>
        <translation>оновлення</translation>
    </message>
</context>
<context>
    <name>YaicsApp</name>
    <message>
        <source>E&amp;xit</source>
        <translation>В&amp;ихід</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <source>&amp;Reload timeline</source>
        <translation>&amp;Перезавантажити стрічку</translation>
    </message>
    <message>
        <source>Load &amp;older in timeline</source>
        <translation>Завантажити &amp;попередні записи</translation>
    </message>
    <message>
        <source>&amp;Pause home timeline</source>
        <translation>&amp;Призупинити стрічку</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>&amp;Про програму</translation>
    </message>
    <message>
        <source>New &amp;direct message</source>
        <translation>Нове &amp;приватне повідомлення</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Допомога</translation>
    </message>
    <message>
        <source>&lt;p&gt;Yaics is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p&gt;Yaics is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.&lt;/p&gt;&lt;p&gt;You should have received a copy of the GNU General Public License along with Yaics.  If not, see &lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;http://www.gnu.org/licenses/&lt;/a&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Yaics – вільне програмне забезпечення: ви можете розповсюджувати та/або модифікувати його на умовах GNU General Public License, опублікованої Free Software Foundation, версії 3, або (на ваш розсуд) будь-якої більш пізньої версії.&lt;/p&gt;&lt;p&gt;Yaics розповсюджується із сподіванням, що вона виявиться корисною, але БЕЗ БУДЬ-ЯКИХ ҐАРАНТІЙ; без навіть уявної ґарантії КОМЕРЦІЙНОЇ ПРИДАТНОСТІ чи ВІДПОВІДНОСТІ БУДЬ-ЯКОМУ ПЕВНОМУ ЗАСТОСУВАННЮ.  Зверніться до GNU General Public License за подробицями.&lt;/p&gt;&lt;p&gt;Ви мали отримати копію GNU General Public License разом з Yaics.  Якщо Ви не отримали копії ліцензії, зверніться до &lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;http://www.gnu.org/licenses/&lt;/a&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Copyright &amp;copy; 2010-2015 Mats Sj&amp;ouml;berg</source>
        <translation></translation>
    </message>
    <message>
        <source>Copyright &amp;copy; 2014-2015 Stig Atle Steffensen</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;p&gt;Report bugs and feature requests at </source>
        <translation>&lt;p&gt;Про помилки та побажання повідомляйте до</translation>
    </message>
    <message>
        <source>A simple Qt-based GNU social client.</source>
        <translation>Простий клієнт GNU social на Qt.</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation>Про %1</translation>
    </message>
    <message>
        <source>&amp;Public</source>
        <translation>&amp;Загальне</translation>
    </message>
    <message>
        <source>&amp;Home</source>
        <translation>&amp;Головна</translation>
    </message>
    <message>
        <source>&amp;Mentions</source>
        <translation>&amp;Згадки</translation>
    </message>
    <message>
        <source>P&amp;rofile</source>
        <translation>П&amp;рофіль</translation>
    </message>
    <message>
        <source>&amp;Inbox</source>
        <translation>&amp;Вхідні</translation>
    </message>
    <message>
        <source>&amp;Outbox</source>
        <translation>&amp;Надіслані</translation>
    </message>
    <message>
        <source>&amp;New notice</source>
        <translation>&amp;Новий допис</translation>
    </message>
    <message>
        <source>Conversation</source>
        <translation>Балачка</translation>
    </message>
    <message>
        <source>&amp;Bugtracker</source>
        <translation>&amp;Багтрекер</translation>
    </message>
    <message>
        <source>Timelines are paused.</source>
        <translation>Стрічки призупинено.</translation>
    </message>
    <message>
        <source>Timelines are resumed.</source>
        <translation>Стрічки поновлено.</translation>
    </message>
</context>
<context>
    <name>YaicsSettingsDialog</name>
    <message>
        <source>Yaics restart is mandatory for settings to take effect.
Please restart Yaics.</source>
        <translation>Для застосування налаштувань необхідний перезапуск Yaics.
Перезапустіть, будь ласка, Yaics.</translation>
    </message>
</context>
</TS>
